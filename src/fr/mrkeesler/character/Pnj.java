package fr.mrkeesler.character;

import fr.mrkeesler.base.Line;
import fr.mrkeesler.dialog.DialogFile;
import fr.mrkeesler.dialog.DialogLine;
import fr.mrkeesler.dialog.DialogSearcher;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.*;

public class Pnj {

    private DialogFile file;
    private String name;
    private Line currentLine;

    public Pnj(String fileName, String name){
        this.file = new DialogFile(fileName);
        this.name = name;
        this.currentLine = DialogSearcher.getLine(this.file.getFirstCode(),this.file.getLines());
    }

    public Pnj(String jsonName){
        File json = new File("res/characters/"+jsonName+".json");
        InputStream is;
        try {
            is = new FileInputStream(json);
            JsonReader jsonReader = Json.createReader(is);
            JsonObject obj = jsonReader.readObject();
            jsonReader.close();
            this.file = new DialogFile(obj.getString("dialogFile"));
            this.name = obj.getString("name");
            this.currentLine = DialogSearcher.getLine(this.file.getFirstCode(),this.file.getLines());
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }

    public void speak(){
        System.out.println(name+" says : "+currentLine.getMessage());
        currentLine = DialogSearcher.getLine(currentLine.getNextCode(),file.getLines());
    }
}
