package fr.mrkeesler.dialog;

import java.util.ArrayList;
import java.util.List;

public class DialogLineGenerator {

    public static DialogLine generateLine(String[] res){
        return new DialogLine(res[0],res[1],res[2]);
    }

    public static List<DialogLine> generateAllLine(List<String[]> tab){
        List<DialogLine> res = new ArrayList<>();
        for(String[] s:tab){
            res.add(generateLine(s));
        }
        return res;
    }
}
