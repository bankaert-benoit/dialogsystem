package fr.mrkeesler;

import fr.mrkeesler.character.Pnj;

import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        Pnj pnj = new Pnj("david");
        Scanner sc = new Scanner(System.in);
        String enter = "";
        while(!enter.equalsIgnoreCase("stop")){
            pnj.speak();
            enter = sc.nextLine();
        }
    }
}
